import { RecoilRoot } from "recoil";
import RoutesProvider from "routes";

function App() {
  return (
    <RecoilRoot>
      <RoutesProvider />
    </RecoilRoot>
  );
}

export default App;
