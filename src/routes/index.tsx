import { createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";

const NotFoundPage = () => (<>Not Found</>)

const router = createBrowserRouter([
    {
        path: '/',
        element: <NotFoundPage />,
        children: []
    },
    {
      path: '*',
      element: <NotFoundPage />,
    },
],
{
  basename: "/"
})

export default function RoutesProvider () {
    return <RouterProvider router={router} future={{ v7_startTransition: true }} />
}
