import axios, { AxiosRequestConfig } from 'axios';

interface IOptionRequest extends Omit<AxiosRequestConfig, 'url'> {
    isAuth?: boolean;
}

const instance = axios.create({
    baseURL: process.env.REACT_APP_END_POINT || '',
    timeout: 10000,
    // proxy: {
    //     protocol: 'http',
    //     host: '127.0.0.1',
    //     port: Number(process.env.PORT) || 2222,
    //     auth: {
    //       username: 'mikeymike',
    //       password: 'rapunz3l'
    //     }
    //   },
    // headers: {'X-Custom-Header': 'foobar'}
});

instance.interceptors.request.use((config: any) => {
    // console.log('config ', config)
    return config;
}, error => {
    handleError(error);
});

const handleError = (error: any) => {
    if (axios.isAxiosError(error)) {
        console.log('isAxiosError err ', error)
    }
}

instance.interceptors.response.use((response: any) => {
    const { data, status } = response;
    if (status / 100 === 2)
    return data;
}, error => {
    console.log('config response error ', error)
});

export default function request (url: string, options: IOptionRequest = {}) {
    const { method = 'GET', ...rest } = options;

    return instance(url, { method, ...rest });
}